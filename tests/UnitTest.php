<?php

namespace App\Tests;

use App\Entity\Test;
use PHPUnit\Framework\TestCase;

class UnitTest extends TestCase
{
    public function testTrue(): void
    {
        $test = new Test();
        $test->setName('test');
        $test->setDescription('Ceci est un test');
        $this->assertTrue($test->getName() === 'test');
        $this->assertTrue($test->getDescription() === 'Ceci est un test');
    }

    public function testFalse(): void
    {
        $test = new Test();
        $test->setName('test');
        $test->setDescription('Ceci est un test');
        $this->assertFalse($test->getName() === 'test1');
        $this->assertFalse($test->getDescription() === 'Ceci est un test1');
    }
}
